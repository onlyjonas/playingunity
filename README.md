![PlayingUnity.png](https://bitbucket.org/repo/Rqx97n/images/1703450232-PlayingUnity.png)

# Playing Unity Workshop #

## Time Schedule ##

**Wednesday** 

* Welcome
* Unity Introduction 
* Tutorial: Marble Madness, https://unity3d.com/learn/tutorials/projects/roll-ball-tutorial
* Scripting


**Thursday**

* Exploration Games, https://bitbucket.org/onlyjonas/playingunity 
* Unity 2D / Animation, Introduction
* Individual Project Work 

**Friday**

* Audio in Unity
* Individual Project Work 

**Saturday**

* Project presentations


# Unity Resources #

*official unity resources*

* Get the latest Unity3D version here: http://unity3d.com
* Unity3D Manual: http://docs.unity3d.com/Manual/index.html
* Unity3D Tutorials: http://unity3d.com/learn/tutorials
* Unity3D Community (check the forum for specific questions): http://unity3d.com/community
* Unity3D Scripting Reference: http://docs.unity3d.com/ScriptReference
* Unity Asset Store: https://www.assetstore.unity3d.com

### Unity Plugins ###

* UCLA Mesh Generator (create simple meshes from image textures): http://games.ucla.edu/resource/unity-mesh-creator-2
* Point Cloud Viewer, https://www.assetstore.unity3d.com/en/#!/content/19811
* iTween (tweening scripts) https://www.assetstore.unity3d.com/en/#!/content/84

## Tutorials ##
 
* Roll a Ball: https://unity3d.com/learn/tutorials/projects/roll-ball-tutorial
* AwfulMedia: Making a Simple Game in Unity, https://www.youtube.com/watch?v=qwuPiaFU37w



## Game Making Tools ##

### Models ###
* Free Assets: http://opengameart.org
* 3D Modeling: http://blender.org (open source)
* Photogrammetry (Create 3D Models form Photos): https://memento.autodesk.com/try-memento (free)
* Simple 3D Tools: http://www.123dapp.com (free)
* Google SketchUp: http://www.sketchup.com (free)
* Voxel Editor, MagicaVoxel: https://ephtracy.github.io/index.html?page=mv_main (free)
* Voxel Editor, Qubicle: http://www.minddesk.com (commercial)

### Rigging ###
* mixamo auto rigging: https://www.mixamo.com/auto-rigger

### Textures ###
* Image Editing Tool (Photoshop alternative): https://www.gimp.org
* Normal Map Generator (Win only): http://ssbump-generator.yolasite.com
* Crazy Bumb (Win & Mac): http://www.crazybump.com

### Sound ###
* Sound Effects: http://freesound.org
* Sound Editing Tool: http://www.audacityteam.org
* Sound Effect Generator: http://www.bfxr.net

### Text ###
* Free Fonts: http://openfontlibrary.org
* Unity Twine Plugin: https://github.com/daterre/UnityTwine