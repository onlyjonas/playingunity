﻿using UnityEngine;
using System.Collections;

public class EventChangeColor : MonoBehaviour {

	public string eventMessageTag;
	public Color defaultColor;
	public Color newColor;

	void Start () {
		GetComponent<Renderer> ().material.SetColor ("_Color", defaultColor);
	}

	public void ActivateEvent (string eventMsg) {

		if(eventMsg == eventMessageTag)
			GetComponent<Renderer> ().material.SetColor ("_Color", newColor);

	}

	public void DeactivateEvent (string eventMsg) {

		if(eventMsg == eventMessageTag)
			GetComponent<Renderer> ().material.SetColor ("_Color", defaultColor);
	}
}