﻿using UnityEngine;
using System.Collections;

public class EventPlaySound : MonoBehaviour {

	public string eventMessageTag;
	public AudioClip clip;
	
	public void ActivateEvent (string eventMsg) {
		
		if(eventMsg == eventMessageTag)
			AudioSource.PlayClipAtPoint(clip, transform.position);
		
	}
	
	public void DeactivateEvent (string eventMsg) {

			// DO nothing
	}
}
