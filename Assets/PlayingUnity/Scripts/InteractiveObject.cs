﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof (Collider))]
public class InteractiveObject : MonoBehaviour {

	public string highlightText = "click";

	[Header ("Where should the event being sent to?")]
	public string eventMessageTag;
	public bool sendEventToMe;
	public GameObject[] eventTarget;

	void Start () {
		transform.tag = "interactive";	
	}
	
	public void HighlightIt () {
		GuiManager.highlightText = highlightText;
	}

	public void DoIt () {
		
		if (sendEventToMe) {
			gameObject.SendMessage ("ActivateEvent", eventMessageTag);
		}
		foreach  (GameObject target in eventTarget) {
			if (target) target.SendMessage ("ActivateEvent", eventMessageTag);
		}
		
	}
	
	public void StopIt () {

		if (sendEventToMe) {
			gameObject.SendMessage ("DeactivateEvent", eventMessageTag);
		}
		foreach  (GameObject target in eventTarget) {
			if (target) target.SendMessage ("DeactivateEvent", eventMessageTag);
		}

	}
}
