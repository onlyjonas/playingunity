﻿/* Position Reset  * * * * * * * * * * * * * * * * * * *
 * 
 * Resets position if script owner falls down (if his
 * negative position gets bigger than maxYPos)
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * */

using UnityEngine;
using System.Collections;

public class PositionReset : MonoBehaviour {

	public float maxYPos = -10f;
	private Vector3 defaultPos;

	void Start () {
		defaultPos = transform.position;
	}
	
	void Update () {
	
		if (transform.position.y < maxYPos) {
			transform.position = defaultPos;
		}

	}
}
