﻿using UnityEngine;
using System.Collections;

public class EventMoveToTarget : MonoBehaviour {

	public bool active;
	public string eventMessageTag;
	public float speed; 
	public Transform[] moveTargets;
	public bool moveBackAndForth;
	public float delayBetweenDirChange = 1f;

	private bool dirToggle = true;
	private bool pause;
	private int nextTarget = 1;

	void Update () {
		
		if(active && moveTargets.Length > 0) {
			
			// The step size is equal to speed times frame time.
			float step = speed * Time.deltaTime;

			// Move position a step closer to the target.
			transform.position = Vector3.MoveTowards(transform.position, moveTargets[nextTarget].position, step);

			if(Vector3.Distance(transform.position, moveTargets[nextTarget].position) == 0f) {

				if(dirToggle) { // move forward
				
					if(nextTarget < moveTargets.Length-1) {
						nextTarget++;
					} else { 
						if(moveBackAndForth && !pause) StartCoroutine("PauseAndChangeDir");
					}

				} else { // move backward

					if(nextTarget > 0) {
						nextTarget--;
					} else { 
						if(moveBackAndForth && !pause) StartCoroutine("PauseAndChangeDir");
					}
				}


					 

			}
			
		}
	}

	IEnumerator PauseAndChangeDir() {

		pause = true;
		yield return new WaitForSeconds(delayBetweenDirChange);
		dirToggle = !dirToggle;
		pause = false;

	}


	public void ActivateEvent (string eventMsg) {
		if(eventMsg == eventMessageTag)
			active = true;
	}
	
	public void DeactivateEvent (string eventMsg) {
		if(eventMsg == eventMessageTag) 
			active = false;
	}


}
