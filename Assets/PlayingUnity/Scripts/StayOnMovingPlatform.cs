﻿/* STAY ON MOVING PLATFORM * * * * * * * * * * * * * * *
 * Moving platform script makes the player the child object
 * of the platform while the player is in the trigger area
 * of the platform.
 * 
 * NOTE: An additional Collider (as trigger) is needed 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * */

using UnityEngine;
using System.Collections;

public class StayOnMovingPlatform : MonoBehaviour {
	
	void OnTriggerStay (Collider other) {
		other.transform.parent = transform;
	}

	void OnTriggerExit (Collider other) {
		other.transform.parent = null;
	}
}
