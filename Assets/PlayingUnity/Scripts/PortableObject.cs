﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof (Rigidbody))]
[RequireComponent(typeof (Collider))]
public class PortableObject : MonoBehaviour {

	public string highlightText = "take";

	void Start () {
		transform.tag = "portable";	
	}
	
	public void HighlightIt () {
		GuiManager.highlightText = highlightText;
	}
}
