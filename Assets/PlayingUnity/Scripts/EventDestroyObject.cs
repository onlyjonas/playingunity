﻿using UnityEngine;
using System.Collections;

public class EventDestroyObject : MonoBehaviour {
		
		public string eventMessageTag;

		public void ActivateEvent (string eventMsg) {
			
			if (eventMsg == eventMessageTag)
				Destroy (gameObject);
			
		}
		

}
