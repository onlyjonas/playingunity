﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof (Collider))]
public class ConditionalStateObject : MonoBehaviour {

	[Header ("Which object should activate the event?")]
	public GameObject conditionalObject;

	public string highlightTextA = "close";
	public string highlightTextB = "use key to open";

	[Header ("Where should the event being sent to?")]
	public string eventMessageTag;
	public bool sendEventToMe;
	public GameObject[] eventTarget;

	private bool conditionalState = false;

	void Start () {
		transform.tag = "interactive";	
	}
	
	public void HighlightIt () {

		if (GuiManager.pickUpObject == conditionalObject) {
			GuiManager.highlightText = highlightTextB;
			conditionalState = true;
		} else {
			GuiManager.highlightText = highlightTextA;
			conditionalState = false;
		}
	}
	
	public void DoIt () {

		if (conditionalState) {
		
			if (sendEventToMe) {
				gameObject.SendMessage ("ActivateEvent", eventMessageTag);
			}
			foreach (GameObject target in eventTarget) {
				if (target)
					target.SendMessage ("ActivateEvent", eventMessageTag);
			}
		
			conditionalObject.gameObject.SetActive(false);
			conditionalState = false;
			transform.tag = "Untagged";

		}


	}
	
	public void StopIt () {
		// Do Nothing
	}
}

