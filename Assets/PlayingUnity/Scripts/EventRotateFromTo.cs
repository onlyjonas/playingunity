﻿using UnityEngine;
using System.Collections;

public class EventRotateFromTo : MonoBehaviour {

	public string eventMessageTag;
	public Vector3 rotateToEulerAngle;
	public float speed;
	private bool rotateMe = false;
	
	void Update () {

		Vector3 myRot = new Vector3();

		if (rotateMe) {
			myRot.x = Mathf.LerpAngle (transform.eulerAngles.x, rotateToEulerAngle.x, speed * Time.deltaTime);
			myRot.y = Mathf.LerpAngle (transform.eulerAngles.y, rotateToEulerAngle.y, speed * Time.deltaTime);
			myRot.z = Mathf.LerpAngle (transform.eulerAngles.z, rotateToEulerAngle.z, speed * Time.deltaTime);
		
			transform.eulerAngles = myRot;
		
			if(Vector3.Distance(transform.eulerAngles, rotateToEulerAngle) < 0.01) {
				transform.eulerAngles = rotateToEulerAngle;
				rotateMe = false;
			}
		}
	}

	public void ActivateEvent (string eventMsg) {

		if(eventMsg == eventMessageTag)
			rotateMe = true;
		
	}
	
	public void DeactivateEvent (string eventMsg) {

		if (eventMsg == eventMessageTag)
			rotateMe = false;
	}
}
