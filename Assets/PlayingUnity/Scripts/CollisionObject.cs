﻿using UnityEngine;
using System.Collections;

public class CollisionObject : MonoBehaviour {

	[Header ("Who should activate the event?")]
	public bool useNameDetection;
	public string detectionName;
	public bool useTagDetection;
	public string detectionTag;
	public bool onlyOnce;

	[Header ("Where should the event being sent to?")]
	public string eventMessageTag;
	public bool sendEventToMe;
	public bool sendEventToOther;
	public GameObject[] eventTarget;
	
	private bool once = true;

	void OnCollisionEnter(Collision other) {
		
		if(useNameDetection || useTagDetection) {
			
			if(other.gameObject.name == detectionName  || other.gameObject.tag == detectionTag && once ) {
				DoIt(other.gameObject);		
				if(onlyOnce) once = false;
			}
			
		} else {
			
			if( once ) {
				DoIt( other.gameObject);	
				if(onlyOnce) once = false;
			}
			
		}	
		
	}

	void OnCollisionExit(Collision other) {
		
		if(useNameDetection || useTagDetection) {
			
			if(other.gameObject.name == detectionName  || other.gameObject.tag == detectionTag && once ) {
				StopIt(other.gameObject);		
			}
			
		} else {
			
			if( once ) {
				StopIt( other.gameObject);	
			}
			
		}	
		
	}
	
	void DoIt(GameObject otherObj) {
		
		if(sendEventToMe) gameObject.SendMessage("ActivateEvent", eventMessageTag);
		if(sendEventToOther) otherObj.SendMessage("ActivateEvent", eventMessageTag);
		
		foreach (GameObject obj in eventTarget) {
			if(obj) obj.SendMessage("ActivateEvent", eventMessageTag);
		}
		
	}
	
	void StopIt(GameObject otherObj) {
		
		if(sendEventToMe) gameObject.SendMessage("DeactivateEvent", eventMessageTag);
		if(sendEventToOther) otherObj.SendMessage("DeactivateEvent", eventMessageTag);
		
		foreach (GameObject obj in eventTarget) {
			if(obj) obj.SendMessage("DeactivateEvent", eventMessageTag);
		}
		
	}

		
}
