﻿using UnityEngine;
using System.Collections;

public class EventChangeObject : MonoBehaviour {

	public string eventMessageTag;
	public Transform changeToPrefab;
	public bool toggleBetweenObj;

	private bool once = false;
	private Transform changeToObj;

	void ChangeObject() {

		if (!once) {
			transform.GetComponent<Collider> ().enabled = false;
			transform.GetComponent<Rigidbody> ().useGravity = false;
			transform.GetComponent<Renderer> ().enabled = false;
			changeToObj = (Transform) Instantiate (changeToPrefab, transform.position, Quaternion.identity);
			Debug.Log(changeToObj);
			changeToObj.parent = transform.parent;
			if(!toggleBetweenObj) once = true;
		}
	}

	void ChangeBack() {

		if(changeToObj) transform.position = changeToObj.transform.position;
		if(changeToObj) Destroy (changeToObj.gameObject);
		transform.GetComponent<Collider>().enabled = true;
		transform.GetComponent<Rigidbody>().useGravity = true;
		transform.GetComponent<Renderer> ().enabled = true;

	}

	public void ActivateEvent (string eventMsg) {
		
		if (eventMsg == eventMessageTag)
			ChangeObject ();
		
	}
	
	public void DeactivateEvent (string eventMsg) {

		if (eventMsg == eventMessageTag) {
			if (toggleBetweenObj)
						ChangeBack ();
		}

	}
}
