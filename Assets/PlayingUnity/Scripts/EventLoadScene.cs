﻿using UnityEngine;
using System.Collections;

public class EventLoadScene : MonoBehaviour {

	public string eventMessageTag;
	public int nextSceneIndex;

	public void ActivateEvent (string eventMsg) {
		
		if(eventMsg == eventMessageTag)
			Application.LoadLevel(nextSceneIndex);
	}
	
	public void DeactivateEvent (string eventMsg) {
		// Do Nothing
	}
}
