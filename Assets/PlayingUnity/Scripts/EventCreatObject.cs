﻿using UnityEngine;
using System.Collections;

public class EventCreatObject : MonoBehaviour {
	
	public string eventMessageTag;
	public Transform prefabObject;
	public Transform positionEmpty;
	
	public void ActivateEvent (string eventMsg) {
		
		if(eventMsg == eventMessageTag)
			Instantiate (prefabObject, positionEmpty.position, prefabObject.rotation);
		
	}
	
	public void DeactivateEvent (string eventMsg) {
		
		// Do Nothing
	}
}
