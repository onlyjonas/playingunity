﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace UnityStandardAssets.Characters.FirstPerson {
	
	public class PlayerController : MonoBehaviour {

		// Zoom vars
		public float cameraZoomValue = 30f;
		public float zoomSpeed = 0.5f;
		private Camera playerCam;
		private float defaultZoomValue;

		// Pickup object vars
		public float pickUpDistance = 5f;
		public Vector3 holdingPositionOffset;
		private bool holdingObject = false; 
		private GameObject pickUpEmpty;
		private GameObject examineEmpty;
		public GameObject pickUpObject;

		// Interact With Object var
		private GameObject interactWithObject;

		// Examine vars
		public float dragRotSpeed = 150f;

		// Gui vars
		public Text infoText; 

		void Start () {

			// hide cursor
			Cursor.visible = false;

			// get player cam
			playerCam = Camera.main;
			defaultZoomValue = playerCam.fieldOfView;

			// Create Empty to define holding Position 
			// and Empty to define examine Position
			pickUpEmpty = new GameObject("pickUpEmpty");
			pickUpEmpty.transform.position = playerCam.transform.position + holdingPositionOffset;
			pickUpEmpty.transform.parent = playerCam.transform;

			// and Empty to define examine Position
			examineEmpty = new GameObject("examineEmpty");
			examineEmpty.transform.position = playerCam.transform.position + new Vector3(0,0,holdingPositionOffset.z);
			examineEmpty.transform.parent = playerCam.transform;

		}

		void Update () {
		
			/* holding object * * * * */
			
			if (holdingObject) { 
				
				// left mouse button 
				if (Input.GetButtonDown ("Fire1")) { 
					ReleaseObject ();
				}
				
				// holding right mouse button
				if (Input.GetButton ("Fire2")) { 
					ExamineObject (true);
				} else if (Input.GetButtonUp("Fire2")) {
					ExamineObject (false);
				}
				
			} 

			/* Zoom in/out Camera * * * * */
			if (Input.GetButton ("Fire2")) { // right mouse button
				ZoomCamera (true);
			} else {
				ZoomCamera (false);
			}


			/* check if an object is close to interact with * * * * */

			RaycastHit hit;
			Debug.DrawRay (playerCam.transform.position, playerCam.transform.forward*pickUpDistance, Color.red);

			if (Physics.Raycast (playerCam.transform.position, playerCam.transform.forward, out hit, pickUpDistance)) {		        

				// object in view

				if (hit.transform.tag == "portable") {

					HighlightObject (true, hit.transform.gameObject);

					if (Input.GetButtonDown ("Fire1")) PickUp(hit.transform.gameObject);

				} else if (hit.transform.tag == "interactive"){

					
					if (Input.GetButton ("Fire1")) HighlightObject(false);
					else HighlightObject (true, hit.transform.gameObject);

					if (Input.GetButtonDown ("Fire1")) Interact(true, hit.transform.gameObject);

					if (Input.GetButtonUp ("Fire1")) Interact(false, interactWithObject);

				} else {

					HighlightObject (false);
				}

			} else {

				// object not in view

				HighlightObject (false) ;
				if(interactWithObject) Interact(false, interactWithObject);
			
			}
				

		}

		
		void HighlightObject (bool highlight, GameObject hitObj = null) {

			if(highlight) {
				hitObj.SendMessage ("HighlightIt"); 
				if(GuiManager.highlightText == "") GuiManager.highlightText = hitObj.name; 
			} else {
				GuiManager.highlightText = "";
			} 

		}


		void PickUp ( GameObject hitObj) {
				        
			pickUpObject = hitObj;
			pickUpObject.transform.position = pickUpEmpty.transform.position;
			pickUpObject.transform.parent = pickUpEmpty.transform;
			pickUpObject.GetComponent<Rigidbody>().useGravity = false;
			pickUpObject.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
			pickUpObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
			pickUpObject.GetComponent<Collider>().enabled = false;

			holdingObject = true;
			GuiManager.pickUpObject = hitObj;

			HighlightObject (false) ;
		}


		void Interact ( bool active, GameObject hitObj) {

			if (active) {
				interactWithObject = hitObj;
				hitObj.SendMessage ("DoIt");
			} else {
				interactWithObject.SendMessage ("StopIt");
			}
		}


		void ReleaseObject () {

			pickUpObject.GetComponent<Collider>().enabled = true;
			pickUpObject.GetComponent<Rigidbody>().useGravity = true;
			pickUpObject.transform.parent = null;
			pickUpObject.GetComponent<Rigidbody>().AddForce (playerCam.transform.TransformDirection(Vector3.forward) * 150f);
			pickUpObject = null;

			holdingObject = false;
			GuiManager.pickUpObject = null;
		}


		void ZoomCamera(bool zoom) {

			if (zoom) {
				playerCam.fieldOfView = Mathf.Lerp(playerCam.fieldOfView, cameraZoomValue, zoomSpeed * Time.deltaTime);
			} else {
				playerCam.fieldOfView = Mathf.Lerp(playerCam.fieldOfView, defaultZoomValue, zoomSpeed * Time.deltaTime);
			}

		}


		void ExamineObject(bool examine) {

			if (examine) {

				// disable First Person Controller
				GetComponent<FirstPersonController>().enabled = false;

				// move object to center (examine position)
				pickUpObject.transform.position = Vector3.Lerp(pickUpObject.transform.position, examineEmpty.transform.position, zoomSpeed * Time.deltaTime);


				// examine object
				pickUpObject.transform.Rotate ((Input.GetAxis ("Mouse Y") * -dragRotSpeed * Time.deltaTime), 
				                               (Input.GetAxis ("Mouse X") * -dragRotSpeed * Time.deltaTime), 0, Space.World);

			} else {

				// move object back
				pickUpObject.transform.position = pickUpEmpty.transform.position;

				// enable First Person Controller
				GetComponent<FirstPersonController>().enabled = true;
			}
			
		}

	}
}
