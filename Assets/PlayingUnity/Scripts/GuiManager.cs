﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GuiManager : MonoBehaviour {

	// Static Text vars
	public static string highlightText;	
	public static GameObject pickUpObject;
	
	// Gui Fields
	public Text highlightTextField; 
	public Text pickupTextField;
	
	void Update () {

		if (highlightTextField.text != highlightText)
			highlightTextField.text = highlightText;

		if (pickUpObject) {
			if (pickupTextField.text != pickUpObject.name)
				pickupTextField.text = pickUpObject.name;
		} else {
			pickupTextField.text = "";
		}
	} 
	
}
