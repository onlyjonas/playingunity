﻿/* SENSOR SCRIPT * * * * * * * * * * * * * * * * * * * *
Calls an event if an object enters collider (trigger)

useNameDetection: check to activate name detection
detectionName: name of the object that should cause the event  
useTagDetection: check to activate tag detection
detectionTag: tag of objects that should cause the event
onlyOnce: check to cause the event only once
eventMessageTag: message tag that should be send with 
the event call
sendEventToMe: check to send event call to the owner of 
this script
sendEventToOther: check to send event call to collision obj
sendEventToOtherObj: add objects into the list to send them 
the event call 

Note: activate Trigger in Collider
* * * * * * * * * * * * * * * * * * * * * * * * * * * */

using UnityEngine;
using System.Collections;

public class TriggerObject : MonoBehaviour {
	
	[Header ("Who should trigger the event?")]
	public bool useNameDetection;
	public string detectionName;
	public bool useTagDetection;
	public string detectionTag;
	public bool onlyOnce;

	[Header ("Where should the event being sent to?")]
	public string eventMessageTag;
	public bool sendEventToMe;
	public bool sendEventToOther;
	public GameObject[] eventTarget;

	private bool once = true;

	
	void OnTriggerEnter(Collider other) {
		
		if(useNameDetection || useTagDetection) {
			
			if(other.gameObject.name == detectionName  || other.gameObject.tag == detectionTag && once ) {
				DoIt(other.gameObject);		
				if(onlyOnce) once = false;
			}
			
		} else {
			
			if( once ) {
				DoIt( other.gameObject);	
				if(onlyOnce) once = false;
			}
			
		}	
		
	}
	

	void OnTriggerExit(Collider other) {
		
		if(useNameDetection || useTagDetection) {
			
			if(other.gameObject.name == detectionName  || other.gameObject.tag == detectionTag && once ) {
				StopIt(other.gameObject);		
			}
			
		} else {
			
			if( once ) {
				StopIt( other.gameObject);	
			}
			
		}	
		
	}
	
	void DoIt(GameObject otherObj) {
		
		if(sendEventToMe) gameObject.SendMessage("ActivateEvent", eventMessageTag);
		if(sendEventToOther) otherObj.SendMessage("ActivateEvent", eventMessageTag);
		
		foreach (GameObject obj in eventTarget) {
			if(obj) obj.SendMessage("ActivateEvent", eventMessageTag);
		}
		
	}

	void StopIt(GameObject otherObj) {
		
		if(sendEventToMe) gameObject.SendMessage("DeactivateEvent", eventMessageTag);
		if(sendEventToOther) otherObj.SendMessage("DeactivateEvent", eventMessageTag);
		
		foreach (GameObject obj in eventTarget) {
			if(obj) obj.SendMessage("DeactivateEvent", eventMessageTag);
		}
		
	}
}


