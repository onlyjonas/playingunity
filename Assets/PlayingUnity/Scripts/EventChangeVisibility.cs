﻿using UnityEngine;
using System.Collections;

public class EventChangeVisibility : MonoBehaviour {

	public string eventMessageTag;

	public bool defaultActivState;
	private bool active;

	void Start () {
		active = defaultActivState;
		ChangeVisibility (active);
	}

	void ChangeVisibility (bool visible) {

		transform.GetComponent<Collider> ().enabled = visible;
		transform.GetComponent<Renderer> ().enabled = visible;

	}


	public void ActivateEvent (string eventMsg) {
		
		if (eventMsg == eventMessageTag)
			ChangeVisibility (!active);
		
	}
	
	public void DeactivateEvent (string eventMsg) {
		
		if(eventMsg == eventMessageTag)
			ChangeVisibility (active);
	}
}
